package com.miccalsa.minicms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.miccalsa.minicms.domain.model.Customer;
import com.miccalsa.minicms.repository.CustomerRepository;
import com.miccalsa.minicms.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> listCustomers() {
        return this.customerRepository.findAll();
    }

    @Override
    public Customer getCustomer(String id) {
        return this.customerRepository.findById(id).orElse(null);
    }

    @Override
    public Customer createCustomer(Customer customer) {
        return this.customerRepository.save(customer);
    }

    @Override
    public Customer editCustomer(String id, Customer customer) {
        return this.customerRepository.findById(id).map(existing -> {
            existing.setName(customer.getName());
            existing.setContacts(customer.getContacts());
            return this.customerRepository.save(existing);
        }).orElse(null);
    }

    @Override
    public void deleteCustomer(String id) {
        this.customerRepository.deleteById(id);
    }
}
