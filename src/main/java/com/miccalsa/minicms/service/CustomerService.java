package com.miccalsa.minicms.service;

import java.util.List;

import com.miccalsa.minicms.domain.model.Customer;

public interface CustomerService {

    List<Customer> listCustomers();

    Customer getCustomer(String id);

    Customer createCustomer(Customer customer);

    Customer editCustomer(String id, Customer customer);

    void deleteCustomer(String id);
}
