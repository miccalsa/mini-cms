package com.miccalsa.minicms.domain.model;

public class Contact {

    private String name;
    private String phone;
    private String email;
    private String role;

    public Contact() {
    }

    public Contact(String name, String phone, String email, String role) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }

        if (o == null || getClass() != o.getClass()) { return false; }

        Contact contact = (Contact) o;

        return new org.apache.commons.lang3.builder.EqualsBuilder()
            .append(getName(), contact.getName())
            .append(getPhone(), contact.getPhone())
            .append(getEmail(), contact.getEmail())
            .append(getRole(), contact.getRole())
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new org.apache.commons.lang3.builder.HashCodeBuilder(17, 37)
            .append(getName())
            .append(getPhone())
            .append(getEmail())
            .append(getRole())
            .toHashCode();
    }

    @Override
    public String toString() {
        return new org.apache.commons.lang3.builder.ToStringBuilder(this)
            .append("name", name)
            .append("phone", phone)
            .append("email", email)
            .append("role", role)
            .toString();
    }
}
