package com.miccalsa.minicms.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.miccalsa.minicms.domain.model.Customer;

@Repository
public interface CustomerRepository extends MongoRepository<Customer, String> {
}
